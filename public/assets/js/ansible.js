jQuery(document).ready(function() {

    $.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });

    var sub_content = $('.sub-box-content');
    var sub_loading = $('.sub-box-loading');




    $('#form_add_another').hide();

    $('body').on('click', '.btn-add-new', function() {
        $('.add-data-form').show();
        $('.btn-add-new').hide();
    });

    $('body').on('click', '.btn_add_another', function() {

        $('#form_add_another').show();
        $('.btn_add_another').hide();
    });

    $('body').on('click', '.change_status_active', function() {

        var data_type = $(this).closest('li').attr("data-type");
        var data_id = $(this).closest('li').attr("data-id");
        var status = "disable";

        if(confirm("Please confirm Your selection! Did you need to disable this?")){

            var url = '/ansible/change_status';
            $.ajax({
                type: "POST",
                cache: false,
                url: url,
                data: {'data_type': data_type, 'data_id': data_id, 'status': status},
                dataType: "json",
                success: function(res)
                {
                    $('#manin_data_raw_'+data_id+' .change_status_active').hide();
                    $('#manin_data_raw_'+data_id+' .change_status_notactive').show();

                    /* if (Layout.fixContentHeight) {
                     Layout.fixContentHeight();
                     }*/
                    Metronic.initUniform();
                },
                async: false
            });
        }


    });

    $('body').on('click', '.change_status_notactive', function() {

        var data_type = $(this).closest('li').attr("data-type");
        var data_id = $(this).closest('li').attr("data-id");
        var status = "enable";

        if(confirm("Please confirm Your selection! Did you need to enable this?")){

            var url = '/ansible/change_status';
            $.ajax({
                type: "POST",
                cache: false,
                url: url,
                data: {'data_type': data_type, 'data_id': data_id, 'status': status},
                dataType: "json",
                success: function(res)
                {
                    $('#manin_data_raw_'+data_id+' .change_status_active').show();
                    $('#manin_data_raw_'+data_id+' .change_status_notactive').hide();

                    /* if (Layout.fixContentHeight) {
                     Layout.fixContentHeight();
                     }*/
                    Metronic.initUniform();
                },
                async: false
            });
        }


    });

    $('body').on('click', '.remove_main_data', function() {

        var data_type = $(this).closest('li').attr("data-type");
        var data_id = $(this).closest('li').attr("data-id");

        if(confirm("Please confirm Your selection!")){

            var url = '/ansible/remove_maindata';
            $.ajax({
                type: "POST",
                cache: false,
                url: url,
                data: {'data_type': data_type, 'data_id': data_id},
                dataType: "json",
                success: function(res)
                {
                    $('#manin_data_raw_'+data_id).remove();
                    $(".portlet-body .scroller").slimScroll( { height: '300px' } );

                    /* if (Layout.fixContentHeight) {
                         Layout.fixContentHeight();
                     }*/
                    Metronic.initUniform();
                },
                async: false
            });
        }


    });

    $('body').on('click', '.remove_sub_data', function() {

        var data_type = $(this).closest('div').attr("data-remove-type");
        var data_id = $(this).closest('div').attr("data-remove-id");
        var parent_id = $('#id_add_another').val()

        if(confirm("Please confirm Your selection!")){

            var url = '/ansible/remove_subdata';
            $.ajax({
                type: "POST",
                cache: false,
                url: url,
                data: {'data_type': data_type, 'data_id': data_id, 'parent_id': parent_id},
                dataType: "json",
                success: function(res)
                {
                    $('#custom_li_row_'+data_id).remove();

                    if (Layout.fixContentHeight) {
                        Layout.fixContentHeight();
                    }
                    Metronic.initUniform();
                },
                async: false
            });
        }


    });

    $('body').on('click', '.btn-arrow-link', function() {
        $('.scroller').css('height', '');
        $('.slimScrollDiv').css('height', '');
        $('.btn-arrow-less').show();
        $('.btn-arrow-link').hide();

    });

    $('body').on('click', '.btn-arrow-less', function() {
        $('.scroller').css('height', '300px');
        $('.slimScrollDiv').css('height', '300px');
        $('.btn-arrow-link').show();
        $('.btn-arrow-less').hide();

    });

    $('body').on('click', '.ansible_list_raw', function() {

        var data_type = $(this).closest('li').attr("data-type");
        var data_id = $(this).closest('li').attr("data-id");
        $('.feeds li').css('background-color', '#fafafa');
        $(this).closest('li').css('background-color', '#00CED1');


        $('#type_add_another').val(data_type);
        $('#id_add_another').val(data_id);

        sub_loading.show();
        sub_content.html('');
        var url = '/ansible/sub_data';

        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: {'data_type': data_type, 'data_id': data_id},
            dataType: "html",
            success: function(res)
            {

                $('.sub-box-loading').hide();
                $('.sub-box-content').html(res);
                $(".portlet-body .scroller").slimScroll( { height: '300px' } );

                /*if (Layout.fixContentHeight) {
                    Layout.fixContentHeight();
                }*/
                Metronic.initUniform();
            },
            async: false
        });
    });

});