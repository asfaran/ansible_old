<?php
define('PAGE_PARENT', 'dashboard', true);
define('PAGE_CURRENT', 'dashboard', true);
?>
@extends('app')

@section('title', 'Dashboard')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Dashboard <small>Ansible Dashboard</small>
</h3>
<!-- END PAGE HEADER-->


<div class="portlet ">
    <div class="">
        <div class="row inbox">
            <div class="col-md-12 portlet light">
                <div class="inbox-header">
                   {{-- <h1 class="pull-left">HOSTS</h1>--}}
                </div>
                <div class="inbox-loading">
                    Loading...
                </div>
                <div class="inbox-content">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>
@endsection