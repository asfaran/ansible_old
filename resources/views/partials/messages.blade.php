<?php $error = $errors->all(); ?>
@if ($error)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert"></button>
        <span><ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul></span>
    </div>
@endif

@if (Session::has('flash_message'))
    @if (Session::has('flash_type') && Session::get('flash_type') == 'success')
        <div class="alert alert-success">
            <span>{!! Session::get('flash_message') !!}</span>
        </div>
    @endif
    @if (Session::has('flash_type') && Session::get('flash_type') == 'error')
        <div class="alert alert-danger">
            <span>{!! Session::get('flash_message') !!}</span>
        </div>
    @endif
@endif