<?php
define('PAGE_PARENT', '/', true);
define('PAGE_CURRENT', 'tasks', true);
?>
@extends('app')

@section('title', 'Dashboard')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Dashboard <small>Ansible Dashboard</small>
</h3>
<!-- END PAGE HEADER-->


<div class="portlet ">
    <div class="">
        <div class="row inbox">
            <div class="col-md-12 portlet light">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-blue-steel hide"></i>
                                        <span class="caption-subject font-blue-steel bold uppercase">Tasks</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                        <ul class="feeds">
                                            @foreach($group_array as $key => $value)
                                                <li>
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-info">
                                                                    {{ $value->id }}
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    {{ $value->name }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="scroller-footer">
                                        <div class="btn-arrow-link pull-right">
                                            <a href="javascript:;">See All Records</a>
                                            <i class="icon-arrow-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="todo-tasklist-devider">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="scroller" style="max-height: 800px;" data-always-visible="0" data-rail-visible="0" data-handle-color="#dae3e7">

                                <div class="sub-box-loading">
                                    Loading...
                                </div>
                                <div class="sub-box-content">
                                </div>

                                <form action="#" class="form-horizontal">
                                    <!-- TASK HEAD -->
                                    <div class="form">
                                        <div class="form-group">
                                            <div class="col-md-8 col-sm-8">
                                                <div class="todo-taskbody-user">
                                                    <span class="todo-username pull-left">Vanessa Bond</span>
                                                    <button type="button" class="todo-username-btn btn btn-circle btn-default btn-xs">&nbsp;edit&nbsp;</button>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="todo-taskbody-date pull-right">
                                                    <button type="button" class="todo-username-btn btn btn-circle btn-default btn-xs">&nbsp; Complete &nbsp;</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END TASK HEAD -->


                                        <!-- TASK TAGS -->
                                        <div class="form-actions right todo-form-actions">
                                            <button class="btn btn-circle btn-sm green-haze">Save Changes</button>
                                            <button class="btn btn-circle btn-sm btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>
@endsection








