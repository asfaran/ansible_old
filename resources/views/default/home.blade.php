<?php
define('PAGE_PARENT', 'dashboard', true);
define('PAGE_CURRENT', 'dashboard', true);
?>
@extends('app')

@section('title', 'Dashboard')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Hotspot Configuration
    <small>Configure Hotspot</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="/">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/app/ezegate/chilli">eZeGate</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="/app/ezegate/chilli">Hotspot</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class=""></i>Hotspot Configuration
                </div>
                <div class="actions">
                    <a href="{{ url('app/ezegate/chilli/update-chilli') }}" class="btn btn-default btn-sm">
                        <i class="fa fa-plus"></i> Create new Hotspot</a>
                </div>
            </div>
            <!-- portlet-body -->
            <div class="portlet-body">
                @include('partials.messages')
                <div >
                    <!-- BEGIN CONDENSED TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-picture"></i>Active Hotspots/Status :</div>
                        </div>
                        <div class="portlet-body">

                        </div>
                    </div>
                    <!-- END CONDENSED TABLE PORTLET-->
                </div>
            </div>
            <!-- portlet-body -->
        </div>


    </div>
</div>
@stop