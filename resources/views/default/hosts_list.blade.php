<?php
define('PAGE_PARENT', '/', true);
define('PAGE_CURRENT', 'hosts', true);
?>
@extends('app')

@section('title', 'Dashboard')

@section('content')
        <!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Dashboard <small>Ansible Dashboard</small>
</h3>
<!-- END PAGE HEADER-->


<div class="portlet ">
    <div class="">
        <div class="row inbox">
            <div class="col-md-12 portlet light">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-blue-steel hide"></i>
                                        <span class="caption-subject font-blue-steel bold uppercase">Hosts</span>
                                    </div>
                                    <button class="btn btn-md green filter-cancel pull-right btn-add-new">
                                        <i class="fa fa-plus"></i>
                                        Add a Host
                                    </button>
                                </div>
                                <div class="portlet-body">
                                    @include('partials.messages')
                                    <div class="form add-data-form" style="display: none">
                                        <form method="post" action="/ansible/add_host" class="form-horizontal" >
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Host IP</label>
                                                    <div class="col-md-6">
                                                        <input class="form-control" placeholder="Type a host" type="text" name="add_host_host" id="add_host_host" />

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Host Name</label>
                                                    <div class="col-md-6">
                                                        <input class="form-control" placeholder="Type a host name" type="text" name="add_host_host_name" id="add_host_host_name" />

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-actions right todo-form-actions">
                                                    <button class="btn btn-circle btn-sm green-haze">Create Host</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                        <ul class="feeds">
                                            @foreach($host_array as $key => $value)
                                                <li
                                                    @if($raw_id == $value->id ) style="background-color:#00CED1;" @endif
                                                    id="manin_data_raw_{{ $value->id }}"
                                                    data-type="host"
                                                    data-id="{{ $value->id }}">
                                                    <div class="col1 ansible_list_raw">
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <div class="label label-sm label-info">
                                                                    {{ $value->id }}
                                                                </div>
                                                            </div>
                                                            <div class="cont-col2">
                                                                <div class="desc">
                                                                    {{ $value->hostname }}
                                                                    <span class="pull-right">{{ $value->host }}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col2"  >
                                                        <div class="cont">
                                                            <div class="cont-col1">
                                                                <button class="btn btn-sm btn-success filter-cancel pull-left change_status_active "
                                                                        @if($value->enabled == 0 ) style="display: none"  @endif title="Disable This" >
                                                                    <i class="fa   fa-check-circle-o"></i>
                                                                </button>
                                                                <button class="btn btn-sm btn-warning filter-cancel pull-left change_status_notactive "
                                                                        @if($value->enabled == 1 ) style="display: none"  @endif title="Enable This" >
                                                                    <i class="fa  fa-ban"></i>
                                                                </button>

                                                            </div>
                                                            <div class="cont-col2">
                                                                <button class="btn btn-sm btn-danger filter-cancel pull-right remove_main_data" title="Delete This Host">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="scroller-footer">
                                        <div class="btn-arrow-link pull-right">
                                            <a href="javascript:;">See All Records</a>
                                            <i class="icon-arrow-right"></i>
                                        </div>
                                        <div class="btn-arrow-less pull-right display-hide">
                                            <a href="javascript:;">See less</a>
                                            <i class="icon-arrow-up"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="todo-tasklist-devider">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            @if($raw_id !== null)
                                <div class="sub-box-content" style="height: 300px;">
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="icon-share font-blue-steel hide"></i>
                                                            <span class="caption-subject font-blue-steel bold uppercase">Groups For Selected Host </span>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                                            <ul class="feeds">
                                                                @if(count($group_array) > 0)
                                                                    @foreach($group_array as $key => $value)
                                                                        <li  id="custom_li_row_{{ $value->id }}" >
                                                                            <div class="col1">
                                                                                <div class="cont">
                                                                                    <div class="cont-col1">
                                                                                        <div class="label label-sm label-info">
                                                                                            {{ $value->id }}
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="cont-col2">
                                                                                        <div class="desc">
                                                                                            {{ $value->name }}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col2">
                                                                                <div class="date" data-remove-type="group"  data-remove-id="{{ $value->id }}" >
                                                                                    <button class="btn btn-sm red filter-cancel remove_sub_data">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                @else
                                                                    <li>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-sm label-info">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc alert-warning">
                                                                                        No Groups Found!
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom btn_add_another">
                                                <i class="fa fa-plus"></i>
                                                Add Groups
                                            </button>
                                        </div>
                                    </div>


                                    <form  method="post" action="/ansible/" class="form-horizontal" id="form_add_another" style="display: none">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <!-- TASK HEAD -->
                                        <div class="form">
                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                                    <div class="form-group">
                                                        <input class="form-control" value="host" type="hidden" name="type_add_another" id="type_add_another" />
                                                        <input class="form-control" value="{{ $raw_id }}" type="hidden" name="id_add_another" id="id_add_another" />
                                                        <select multiple class="form-control"  name="value_add_another[]" id="value_add_another"  size="9" >
                                                            @foreach($balance_groups as $group_key => $group_value)
                                                                <option value="{{ $group_value->id }}">{{ $group_value->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END TASK HEAD -->


                                            <!-- TASK TAGS -->
                                            <div class="form-actions right todo-form-actions">
                                                <button class="btn btn-circle btn-sm green-haze">Add Group To Host</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @else
                                <div class="sub-box-loading" style="height: 20px;">
                                    <h4>Host Still Not Selected!</h4>
                                </div>
                                <div class="sub-box-content" style="height: 300px;">
                                </div>
                            @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>
@endsection
