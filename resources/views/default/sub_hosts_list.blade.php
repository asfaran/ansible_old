
<div class="portlet-body">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light ">
                <!----- End Listing Parent Group of selected group --->
                @if(count($parent_groups) > 0)
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-share font-blue-steel hide"></i>
                            <span class="caption-subject font-blue-steel bold uppercase">Selected Group Belongs to </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="feeds">
                            @foreach($parent_groups as $key => $value)
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    {{ $value->id }}
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    {{ $value->name }}
                                                    <div class="pull-right">
                                                         Parent Group
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!----- End Listing Parent Group of selected group --->
                <!----- Start Listing Child Group of selected group --->
                @if(count($child_groups) > 0)
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-blue-steel hide"></i>
                                <span class="caption-subject font-blue-steel bold uppercase">This Child Group Blongs to Selected Group.</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <ul class="feeds">
                                @foreach($child_groups as $key => $value)
                                    <li>
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        {{ $value->id }}
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        {{ $value->name }}
                                                        <div class="pull-right">
                                                            (Child)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                @endif
                <!----- End Listing Child Group of selected group --->


                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue-steel hide"></i>
                        <span class="caption-subject font-blue-steel bold uppercase">Hosts For Selected Group </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            @if(count($host_array) > 0)
                                @foreach($host_array as $key => $value)
                                    <li id="custom_li_row_{{ $value->id }}" >
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        {{ $value->id }}
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        {{ $value->hostname }}
                                                        <div class="pull-right">
                                                            --{{ $value->host }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date" data-remove-type="host"  data-remove-id="{{ $value->id }}">
                                                <button class="btn btn-sm red filter-cancel remove_sub_data">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc alert-warning">
                                                    No Hosts Found!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="form-group">
    <div class="col-md">
        <button class="btn btn-sm yellow filter-submit margin-bottom btn_add_another">
            <i class="fa fa-plus"></i>
            Add Another Host
        </button>
    </div>
</div>

<form method="post" action="/ansible/" class="form-horizontal"  id="form_add_another" style="display: none">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- TASK HEAD -->
    <div class="form">
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div class="form-group">
                    <input class="form-control" value="{{ $data_type }}" type="hidden" name="type_add_another" id="type_add_another" />
                    <input class="form-control" value="{{ $data_id }}" type="hidden" name="id_add_another" id="id_add_another" />
                    <select multiple class="form-control"  name="value_add_another[]" id="value_add_another"  size="9" >
                        @foreach($balance_hosts as $host_key => $host_value)
                            <option value="{{ $host_value->id }}">{{ $host_value->hostname }}</option>
                        @endforeach
                    </select>

                </div>
            </div>
        </div>
        <!-- END TASK HEAD -->


        <!-- TASK TAGS -->
        <div class="form-actions right todo-form-actions">
            <button class="btn btn-circle btn-sm green-haze">Add Host To Group</button>
        </div>
    </div>
</form>