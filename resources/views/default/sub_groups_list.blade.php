
<div class="portlet-body">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue-steel hide"></i>
                        <span class="caption-subject font-blue-steel bold uppercase">Groups For Selected Host </span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            @if(count($group_array) > 0)
                                @foreach($group_array as $key => $value)
                                    <li  id="custom_li_row_{{ $value->id }}" >
                                        <div class="col1">
                                            <div class="cont">
                                                <div class="cont-col1">
                                                    <div class="label label-sm label-info">
                                                        {{ $value->id }}
                                                    </div>
                                                </div>
                                                <div class="cont-col2">
                                                    <div class="desc">
                                                        {{ $value->name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col2">
                                            <div class="date" data-remove-type="group"  data-remove-id="{{ $value->id }}" >
                                                <button class="btn btn-sm red filter-cancel remove_sub_data">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc alert-warning">
                                                    No Groups Found!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="form-group">
    <div class="col-md">
        <button class="btn btn-sm yellow filter-submit margin-bottom btn_add_another">
            <i class="fa fa-plus"></i>
                Add Groups
        </button>
    </div>
</div>


<form  method="post" action="/ansible/" class="form-horizontal" id="form_add_another" style="display: none">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- TASK HEAD -->
    <div class="form">
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div class="form-group">
                    <input class="form-control" value="{{ $data_type }}" type="hidden" name="type_add_another" id="type_add_another" />
                    <input class="form-control" value="{{ $data_id }}" type="hidden" name="id_add_another" id="id_add_another" />
                    <select multiple class="form-control"  name="value_add_another[]" id="value_add_another"  size="9" >
                         @foreach($balance_groups as $group_key => $group_value)
                             <option value="{{ $group_value->id }}">{{ $group_value->name }}</option>
                         @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!-- END TASK HEAD -->


        <!-- TASK TAGS -->
        <div class="form-actions right todo-form-actions">
            <button class="btn btn-circle btn-sm green-haze">Add Group To Host</button>
        </div>
    </div>
</form>