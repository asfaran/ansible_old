<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('home', 'DefaultController@home');
Route::get('/dashboard', 'DefaultController@index');
Route::get('host_list', 'DefaultController@HostsList');
Route::get('groups_list', 'DefaultController@GroupsList');
Route::get('tasks_list', 'DefaultController@TasksList');
Route::any('sub_data', 'DefaultController@SubData');
Route::any('remove_subdata', 'DefaultController@RemoveSubData');
Route::any('remove_maindata', 'DefaultController@RemoveMainData');

Route::any('change_status', 'DefaultController@ChangeStatus');

Route::post('add_group', 'DefaultController@AddGroup');
Route::post('add_host', 'DefaultController@AddHost');

Route::any('groups_list/{raw_id}', 'DefaultController@GroupsList');
Route::any('host_list/{raw_id}', 'DefaultController@HostsList');

Route::get('/', 'DefaultController@index');
Route::post('/', 'DefaultController@AddAnother');


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
