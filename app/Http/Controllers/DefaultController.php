<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Session;

class DefaultController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('guest');
    }*/

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return Redirect::to('/host_list');
    }


    public function AddAnother()
    {
        $data_type = Input::get('type_add_another');
        $data_id = Input::get('id_add_another');
        $value = Input::get('value_add_another');

        if($data_type == "host"){


            foreach ($value as $key => $id) {

                DB::table('hostgroups')->insert(
                    array('host_id' => $data_id , 'group_id' => $id)
                );
            }

            return Redirect::to('/host_list/'.$data_id);

        }elseif($data_type == "group"){

            foreach ($value as $key => $id) {

                DB::table('hostgroups')->insert(
                    array('group_id' => $data_id , 'host_id' => $id)
                );
            }

            return Redirect::to('/groups_list/'.$data_id);

        }





    }



    public function home()
    {
        return view('welcome');
    }


    public function HostsList($raw_id = null)
    {
        $hosts = DB::select("select * from host ");

        if($raw_id !== null){

            $groups = DB::select("SELECT gp.id, gp.name  FROM `group` AS gp  INNER JOIN hostgroups ON hostgroups.group_id = gp.id  where hostgroups.host_id = $raw_id  GROUP BY gp.id ");
            $balance_groups = DB::select("SELECT gp.id, gp.name FROM `group` AS gp  WHERE gp.id NOT IN (SELECT group_id FROM hostgroups WHERE  host_id = $raw_id )  GROUP BY gp.id ");


            if(count($balance_groups) == 0 || !$balance_groups || count($balance_groups) == null){

                $balance_groups = DB::select("select id, name from `group` ");
            }

        }else{
            $groups = array();
            $balance_groups = array();
        }

        $data = [
            'host_array' => $hosts,
            'raw_id' => $raw_id,
            'balance_groups' => $balance_groups,
            'group_array' => $groups
        ];
        return view('default.hosts_list', $data);
    }


    public function AddHost()
    {
        /*if (Request::isMethod('POST')) {*/

            $validator = Validator::make(
                [
                    'host'=>Input::get('add_host_host'),
                    'host_name'=>Input::get('add_host_host_name')
                ],
                [
                    'host' => 'required',
                    'host_name' => 'required',
                ]
            );


            if ($validator->fails()) {
                return Redirect::to('/host_list')->withInput()->withErrors($validator);

            }else{
                $host = Input::get('add_host_host');
                $host_name = Input::get('add_host_host_name');

                DB::table('host')->insert(
                    array('host' => $host, 'hostname' => $host_name)
                );

                Session::flash('flash_message', 'Host Add Successfully');
                Session::flash('flash_type', 'success');
                return Redirect::to('/host_list');

            }
        /*}*/

    }


    public function GroupsList($raw_id = null)
    {
        $groups = DB::select("select * from `group` ");

        if($raw_id !== null){

            $hosts = DB::select("SELECT host.id, host.host, host.hostname FROM host  INNER JOIN hostgroups ON hostgroups.host_id = host.id  where hostgroups.group_id = $raw_id GROUP BY host.id ");
            $balance_hosts = DB::select("SELECT host.id, host.host, host.hostname FROM host  WHERE host.id NOT IN (SELECT host_id FROM hostgroups WHERE  group_id = $raw_id ) GROUP BY host.id ");
            $parent_groups = DB::select("SELECT `group`.id, `group`.name  FROM `group`  INNER JOIN childgroups ON childgroups.parent_id = `group`.id  where childgroups.child_id = $raw_id   GROUP BY childgroups.id ");
            $child_groups = DB::select("SELECT `group`.id, `group`.name  FROM `group`  INNER JOIN childgroups ON childgroups.child_id = `group`.id  where childgroups.parent_id = $raw_id  GROUP BY childgroups.id ");

            if(count($balance_hosts) == 0 || !$balance_hosts || count($balance_hosts) == null){

                $balance_hosts = DB::select("select id, host, host.hostname from host ");
            }

        }else{
            $hosts = array();
            $balance_hosts = array();
            $parent_groups = array();
            $child_groups = array();
        }

        $data = [
            'group_array' => $groups,
            'raw_id' => $raw_id,
            'balance_hosts' => $balance_hosts,
            'host_array' => $hosts,
            'parent_groups' => $parent_groups,
            'child_groups' => $child_groups
        ];

        return view('default.groups_list', $data);
    }

    public function AddGroup(Request $request)
    {
        /*if (Request::isMethod('POST')) {*/

            $validator = Validator::make(
                [
                    'add_another_field'=>Input::get('add_another_field')
                ],
                [
                    'add_another_field' => 'required',
                ]
            );


            if ($validator->fails()) {
                return Redirect::to('/groups_list')->withInput()->withErrors($validator);

            }else{
                $group_name = Input::get('add_another_field');

                DB::table('group')->insert(
                    array('name' => $group_name)
                );

                Session::flash('flash_message', 'Group Add Successfully');
                Session::flash('flash_type', 'success');
                return Redirect::to('/groups_list');

            }
        /*}*/
    }


    public function TasksList()
    {
        /*$inventory = DB::select("select * from inventory ");

        $data = [
            'inventory_array' => $inventory
        ];*/

        $groups = DB::select("select * from `group` ");

        $data = [
            'group_array' => $groups
        ];
        return view('default.tasks_list', $data);
    }




    public function RemoveMainData()
    {

        $data_type = Input::get('data_type');
        $data_id = Input::get('data_id');

        if($data_type == "host"){
            DB::table('hostgroups')->where('host_id', '=', $data_id)->delete();
            DB::table('host')->where('id', '=', $data_id)->delete();
            return 1;


        }elseif($data_type == "group"){
            DB::table('hostgroups')->where('group_id', '=', $data_id)->delete();
            DB::table('group')->where('id', '=', $data_id)->delete();
            return 1;

        }

    }


    public function ChangeStatus()
    {

        $data_type = Input::get('data_type');
        $data_id = Input::get('data_id');
        $status = Input::get('status');

        if($status == "enable"){
            $enable_status= 1;
        }elseif($status == "disable"){
            $enable_status= 0;
        }

        if($data_type == "host"){
            /*DB::table('hostgroups')->where('host_id', '=', $data_id)->delete();*/
            DB::table('host')->where('id', '=', $data_id)->update(array('enabled' => $enable_status));
            return 1;


        }elseif($data_type == "group"){
            /*DB::table('hostgroups')->where('group_id', '=', $data_id)->delete();*/
            DB::table('group')->where('id', '=', $data_id)->update(array('enabled' => $enable_status));
            return 1;

        }

    }



    public function RemoveSubData()
    {

        $data_type = Input::get('data_type');
        $data_id = Input::get('data_id');
        $parent_id = Input::get('parent_id');



        if($data_type == "host"){
            DB::table('hostgroups')->where('host_id', '=', $data_id)->where('group_id', '=', $parent_id)->delete();
            return 1;


        }elseif($data_type == "group"){

            DB::table('hostgroups')->where('host_id', '=', $parent_id)->where('group_id', '=', $data_id)->delete();
            return 1;

        }

    }

    public function SubData()
    {

        $data_type = Input::get('data_type');
        $data_id = Input::get('data_id');



        if($data_type == "host"){
            $groups = DB::select("SELECT gp.id, gp.name   FROM `group` AS gp  INNER JOIN hostgroups ON hostgroups.group_id = gp.id  where hostgroups.host_id = $data_id  GROUP BY gp.id ");
            $balance_groups = DB::select("SELECT gp.id, gp.name FROM `group` AS gp  WHERE gp.id NOT IN (SELECT group_id FROM hostgroups WHERE  host_id = $data_id )  GROUP BY gp.id ");

            if(count($balance_groups) == 0 || !$balance_groups || count($balance_groups) == null){

                $balance_groups = DB::select("select id, name from `group` ");
            }

            $data = [
                'group_array' => $groups,
                'balance_groups' => $balance_groups,
                'data_type' => $data_type,
                'data_id' => $data_id
            ];


            return view('default.sub_groups_list', $data);


        }elseif($data_type == "group"){

            $hosts = DB::select("SELECT host.id, host.host, host.hostname FROM host  INNER JOIN hostgroups ON hostgroups.host_id = host.id  where hostgroups.group_id = $data_id GROUP BY host.id ");

            $balance_hosts = DB::select("SELECT host.id, host.host, host.hostname FROM host  WHERE host.id NOT IN (SELECT host_id FROM hostgroups WHERE  group_id = $data_id ) GROUP BY host.id ");
            $parent_groups = DB::select("SELECT `group`.id, `group`.name  FROM `group`  INNER JOIN childgroups ON childgroups.parent_id = `group`.id  where childgroups.child_id = $data_id   GROUP BY childgroups.id ");

            $child_groups = DB::select("SELECT `group`.id, `group`.name  FROM `group`  INNER JOIN childgroups ON childgroups.child_id = `group`.id  where childgroups.parent_id = $data_id   GROUP BY childgroups.id ");

            if(count($balance_hosts) == 0 || !$balance_hosts || count($balance_hosts) == null){

                $balance_hosts = DB::select("select id, host, host.hostname from host ");
            }


            $data = [
                'host_array' => $hosts,
                'balance_hosts' => $balance_hosts,
                'data_type' => $data_type,
                'data_id' => $data_id,
                'parent_groups' => $parent_groups,
                'child_groups' => $child_groups
            ];


            return view('default.sub_hosts_list', $data);

        }

    }
}
